# TimerAPC

A minimal but versatile dual astable derived from an Atari Punk Console (APC). The first astable of the APC is duplicated.

See [Dual timer build](https://gr33nonline.wordpress.com/2019/07/29/dual-timer-build/)